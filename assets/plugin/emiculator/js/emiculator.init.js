/*!

=========================================================
* EMIculator - The EMI Calculator Widget v1.0
=========================================================

* Demo Page: http://prolancer.xyz/emiculator
* Copyright 2019 creativeAb (https://themeforest.net/user/creativeab)
 
* Coded by creativeAb (https://themeforest.net/user/creativeab)

=========================================================

*  Instantly show EMI calculator on your website.

*/

'use strict';

$(document).ready(function(){ 
	$(".emic-calculator").emiculator({
		language : {
			LABEL_PRINICIPAL : "Your Loan Amount is ",
			CALCULATOR_HEADING : "EMI Calculator for Loans ",
		}
	}); 
})